package com.nuuptech.springboot.service.mySpringboot;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Service {

    @RequestMapping("/hello")
    public String greeting() {
        return "Hello Spring";
    }
}
